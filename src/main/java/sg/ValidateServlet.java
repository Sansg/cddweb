package sg;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/ValidateServlet")
public class ValidateServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		LoginDao ld=new LoginDao();
		try {
			if(ld.check(username, password)) 
			{
				HttpSession session=request.getSession();
				session.setAttribute("username", username);
				response.sendRedirect("WelcomePage.jsp");
			}
			else
			{
				response.sendRedirect("LoginPage.jsp");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
