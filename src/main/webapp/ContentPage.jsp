<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Content Page</title>
</head>
<body bgcolor="green">
	<%
		response.setHeader("Cache-Control","no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires","0");
		if(session.getAttribute("username")==null)
		{
			response.sendRedirect("LoginPage.jsp");
		}
	%>
	<center><h2>This is the content page for authenticated users.</h2></center>
	<br>
	<p><h4>CONTENT WILL BE UPDATED SOON..</h4></p>
	<a href="AboutUs.jsp">Click here for more info.</a>
	<br>
	<form action="Logout" method="post">
		<input type="submit" value="Logout">
	</form>
</body>
</html>