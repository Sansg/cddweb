<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome Page</title>
</head>
<body bgcolor="cyan">
	<%
		response.setHeader("Cache-Control","no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires","0");
		if(session.getAttribute("username")==null)
		{
			response.sendRedirect("LoginPage.jsp");
		}
	%>
	<center><h1>Welcome  ${username }..</h1></center>
	<br>
	<p><h3>Information for authenticated user</h3></p>
	<p><a href="ContentPage.jsp">click here</a></p>
	<br>
	<p><h3>For more info.</h3></p>
	<p><a href="AboutUs.jsp">Click here</a></p>
	<form action="Logout" method="post">
		<input type="submit" value="Logout">
	</form>
</body>
</html>